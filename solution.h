#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>
#include <vector>
#include "input.h"
#include "output.h"
using namespace std;

#ifndef FHCP_CHALLENGE_SOLUTION_H
#define FHCP_CHALLENGE_SOLUTION_H

class Solution : public Output {
public:

	// Construtor
	Solution(const Input* p_in);

	void generateNaiveSolution( );

	void generateBetterSolution( );

	vector<int> inserida;

	friend ostream& operator<<(ostream&, const Solution &s);

};

#endif  // FHCP_CHALLENGE_