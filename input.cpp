#include "input.h"

// Construtor padrao
Input::Input(){
   cidades_ = 0;
}

// Faz a leitura do arquivo de entrada e armazena as informacoes necessarias
void Input::load(const string& file_name) {
   string str1;
   
   string line;
   int token1;
   int token2;   

   // Manipulador do arquivo de entrada
   ifstream inputFile (file_name);
   if (inputFile.is_open()){
      // Ignora demais comentarios no arquivo
      getline (inputFile,line);
      getline (inputFile,line);
      getline (inputFile,line);

      // Faz a leitura do numero de cidades no mapa
      inputFile>>str1>>str1>>cidades_;

      // Aloca memoria para a matriz de cidades
      matriz.resize(cidades_, vector<unsigned>(cidades_));
      
      // Ignora demais comentarios no arquivo
      inputFile>>str1>>str1>>str1;
      inputFile>>str1;
      
      inputFile>>token1>>token2;
      while ( token1 != -1 ){
         matriz[token1-1][token2-1] = 1;
         matriz[token2-1][token1-1] = 1;
         inputFile>>token1>>token2;
        
      }
      inputFile.close();
   } 
   else 
      throw runtime_error("Impossivel abrir o arquivo");

}

//! Sobrecarda de operador
ostream& operator<<(ostream& os, const Input &in) {
   unsigned i;
   unsigned j;
     os << "graph{"<<endl;
   for (i = 0; i < in.cidadesGet(); i++) 
      for (j = i+1; j < in.cidadesGet(); j++)
         if(in.matriz[i][j])
            os << i+1<< " -- "<< j+1<<endl;
         os << "}";
   return os;
}