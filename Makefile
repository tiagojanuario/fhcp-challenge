#	Tiago Januario
#	January 26, 2016
#	Makefile for FHCP Challenge
#

CC = g++
OBJECTS = solution.o output.o input.o main.o parametros.o
CFLAGS = -O3
LFLAGS = -Wall

hcp : $(OBJECTS)
	$(CC) -o hcp $(OBJECTS) $(LFLAGS)

solution.o: solution.cpp solution.h
	$(CC) -c -o solution.o solution.cpp $(LFLAGS)

output.o : output.h output.cpp
	$(CC) -c -o output.o output.cpp $(LFLAGS)

input.o : input.h input.cpp
	$(CC) -c -o input.o input.cpp $(LFLAGS)

main.o : main.cpp
	$(CC) -c -o main.o main.cpp $(LFLAGS)

parametros.o : parametros.cpp parametros.h
	$(CC) -c -o parametros.o parametros.cpp

clean: 	
	rm hcp *.o 
