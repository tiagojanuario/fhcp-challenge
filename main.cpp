#include "input.h"
#include "solution.h"
#include "parametros.h"
#include <iostream>
using namespace std;

//! Passar os argumentos como linhas de comando
int main(int ac, char * av[]) {

	Parametros::Store(ac,av);

	Input in;
	in.load(Parametros::getInstance());

	Solution s(&in);
	s.generateNaiveSolution();

	cout<<s<<endl;

	return 0;
}

