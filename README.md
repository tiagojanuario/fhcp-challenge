# MATE34 - Tópicos em Inteligência Computacional IV - FHCP Challenge #

**Objetivo**: Implementar uma função para construir uma solução inicial que será utilizada para resolver o Problema do Circuito Hamiltoniano.

### Download ###

* Se você não é familiarizado com o git, clique neste [link](https://bitbucket.org/tiagojanuario/fhcp-challenge/get/b556e1f428d1.zip) e faça o download do repositório.

* Se você já é familiarizado com o git:

    1. Clique no botão Clone.
2. Copie o comando clone (escolha entre os formatos SSH ou HTTPS).
3. Abra o seu terminal.
4. Navegue até a pasta onde você deseja clonar o repositório.
5. Cole o comando que você copiou do Bitbucket, por exemplo:

```
#!c++

    git clone git@bitbucket.org:tiagojanuario/fhcp-challenge.git
```

Se a clonagem for bem sucedida, parecerá um novo subdiretório de nome fhcp-challenge. 

### Compilação ###

Para compilar o fhcp-challenge:

    1. Abra o terminal
2. Navegue até a pasta que contém os arquivos a serem compilador
3. Execute o seguinte comando:

```
#!c++

make
```
Após a compilarão, será criado um arquivo executável de nome hcp.

### Execução ###

Para executar o programa hcp é preciso fornecer um arquivo de entrada, em geral, uma instância de um problema. Arquivos de entrada para o programa hcp podem ser encontrados no seguinte [link](https://www.dropbox.com/sh/n9gvsuavpd7cxjl/AAB0v92ckV2DqlOrMI0YIUGta?dl=0).

Ao executar o programa hcp, forneça a instância utilizando a opção --instance, por exemplo:

```
#!c++

./hcp --instance graph1.hcp
```

### Configuração ###

Não se faz necessária qualquer configuração. Em caso de problemas, entre em contato.

### Execução de testes ###

O código fonte pode ser modificado e recompilado para executar uma variedade de testes. O programa hcp fornece como saída um arquivo no formato *dotty* que pode ser interpretado por um visualizador de grafos. Eis um exemplo de saída:

```
#!c++

graph{
10 -- 44
11 -- 19
11 -- 45
16 -- 52
16 -- 53
19 -- 52
22 -- 38
22 -- 44
26 -- 43
26 -- 60
27 -- 60
27 -- 65
33 -- 43
33 -- 48
38 -- 45
48 -- 53
}
```

Você pode modificar o programa para exibir outros dados, tais como:
* Tempo de execução
* Custo da solução obtida
* etc...

### Visualizar resultados ###

Para visualizar a saída produzida, copie e cole o resultado produzido dentro da caixa de texto que pode ser acessada no link:
http://goo.gl/jtmluE