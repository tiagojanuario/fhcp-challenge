#include "solution.h"

#define vazio -1

Solution::Solution(const Input* pin):Output(pin){
  inserida.resize(in->cidadesGet());
}

/** 
* Esta eh uma implementacao super simples
* de uma funcao que constroi uma solucao
* aproximada para o problema do circuito 
* hamiltoniano.
*
* Esta funcao tem como ideia basica,
* escolher 2 cidades conectadas e
* a partir delas, determinar um caminho 
* maximal.
**/
void Solution::generateNaiveSolution() {
  int cidade1;
  int cidade2;
  int direita;
  int esquerda;

  // escolhe a cidade1 aleatoriamente
  cidade1 = rand()%in->cidadesGet();

  // A cidade2 ainda nao foi identificada
  cidade2 = vazio;

  // Identifica uma cidade2 vizinha a cidade1
  for(int c=0; c < in->cidadesGet() && cidade2 < 0;c++)
    if(in->matriz[cidade1][c]==true)
      cidade2 = c;
    
    // Mensagem de erro se a cidade1 for um vertice isolado
    //if(cidade2 < 0 )
      //throw runtime_error("Grafo disconexo");

    // Insere as cidade1 e cidade2 em um caminho
    cicle[cidade1][cidade2] = true;
    cicle[cidade2][cidade1] = true;

    // Guarde a informacao de que as cidades foram inseridas
    inserida[cidade1] = true;
    inserida[cidade2] = true;

    // Espandir o caminho para a direita a partir da cidade1
    do{
      // A cidade vizinha da direita ainda nao foi identificada
      direita = -1;
      // Identifica uma cidade da direita que seja vizinha da cidade1
      for(int c=0; c < in->cidadesGet() && direita < 0;c++)
        // se esta cidade nao foi inserida no caminho e se elas estiverem conectadas...
        if(in->matriz[cidade1][c]==true && !inserida[c]){
          direita = c;
          //...insira a conexao entre as duas cidades no caminho
          cicle[cidade1][direita] = true;
          cicle[direita][cidade1] = true;
          // insira a cidade da direita no caminho
          inserida[direita] = true;
          // continue a busca a partir da cidade mais aa direita
          cidade1 = direita;
        }
    }while(direita > 0);// repitir ate que nao seja mais possivel continuar

    // Codigo analogo para a cidade 2
    do{
      esquerda = -1;
      for(int c=0; c < in->cidadesGet() && esquerda < 0;c++)
        if(in->matriz[cidade2][c]==true && !inserida[c]){
          esquerda = c;
          cicle[cidade2][esquerda] = true;
          cicle[esquerda][cidade2] = true;
          inserida[esquerda] = true;
          cidade2 = esquerda;
        }
    }while(esquerda > 0);

    //Tenta fechar o cicle com cidade1 e cidade2
    if(in->matriz[cidade2][cidade1]==true){
      cicle[cidade1][cidade2] = true;
      cicle[cidade2][cidade1] = true;
    }

}// Fim do metodo construtivo

void Solution::generateBetterSolution() {
  // todo Implementar a sua versao

}

//! Sobrecarda de operador
ostream& operator<<(ostream& os, const Solution& out) {
  unsigned i;
  unsigned j;

  os << "graph{"<<endl;

  for (i = 0; i < out.in->cidadesGet(); i++) {
    for (j = i+1; j < out.in->cidadesGet(); j++) {
      if(out.cicle[i][j])
      os << i+1<< " -- "<< j+1<<endl;
    }
  }
  os << "}";
  return os;
}