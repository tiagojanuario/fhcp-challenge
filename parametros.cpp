#include "./parametros.h"

Parametros::Parametros() {
	// nothing to do here!
}

double Parametros::GetSeed(){
	try {
		return stod(vm["seed"]);
	} catch (exception& e) {		
		cout<< "\nfile: "<< __FILE__;
		cout<< ", line: "<< __LINE__;
		cout << ", error: " << e.what() << endl;
		return 0;
	}
}

string Parametros::getInstance(){
	try {
		return vm["instance"];
	} catch (exception& e) {		
		cout << "error: " << e.what() << endl;
		vm["instance"] = "0";
		return vm["instance"];
	}
}

int Parametros::GetSize(){
	try {
		return stoi(vm["size"]);
	} catch (exception& e) {		
		cout<< "\nfile: "<< __FILE__;
		cout<< ", line: "<< __LINE__;
		cout << ", error: " << e.what() << endl;
		return 0;
	}
}

string Parametros::GetVersion(){
	return vm["version"];
}

void Parametros::Store(int argc, char * argv[ ]) {
	
	char optstring[] = "i:a:t:vr:dcq";
	int option_index = 0;
	int opcao = 0;

	struct option long_options[] = {
			{ "version", no_argument, 0,		001 },
			{ "size", required_argument, 0,		002 },
			{ "instance", required_argument, 0,	003 },
			{ "seed", required_argument, 0,		004 },
			{ "help", no_argument, 0,		005 },
			{ "h", no_argument, 0,		006 },
			{ 0,0, 0, 0 } };

	opcao = 0;

	while (opcao != -1) {
		opcao = getopt_long(argc, argv, optstring, long_options, &option_index);
		switch (opcao) {
		case 001:
			vm["version"] = "Versao Brasileira, VTI-Rio.";
			break;
		case 002:
		    vm["size"] = string(optarg);
			break;
		case 003:
			vm["instance"] = string(optarg);
			break;
		case 004:
			vm["seed"] = string(optarg);
			break;
		case 005:
		case 006:
			cout<<"write a help.";
			break;
	}
}

}

map<string,string> Parametros::vm;
