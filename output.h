#include <iostream>
#include <string>
#include <fstream>
#include <vector>
#include "input.h"
using namespace std;

#ifndef FHCP_CHALLENGE_OUTUT_H
#define FHCP_CHALLENGE_OUTUT_H

//! The Ouput Class
class Output {
public:
	//! Constructor
	Output(const Input* pin);

	//! Check if the input is feasible
	bool conectado() const;

	//! Verifica a saida forma um circuito
	bool circuito() const;

	// Um apontador para os dados de entrada
	const Input* in;

	//! Matriz que armazena o circuito hamiltoniano
	vector<vector<unsigned> > cicle;

	//! Operator overloading
	friend ostream& operator<<(ostream&, const Output &);

	//! Operator overloading
	friend istream& operator>>(istream&, Output &);
};

#endif  // FHCP_CHALLENGE_