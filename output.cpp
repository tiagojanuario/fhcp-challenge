#include "output.h"

Output::Output(const Input* pin):in(pin){
  // Aloca memoria para a matriz de cidades
      cicle.resize(in->cidadesGet(), vector<unsigned>(in->cidadesGet()));
}

bool Output::conectado() const {
   // todo Implementar
   return false;

}

bool Output::circuito() const {
   // todo Implementar
   return false;

}

//! Sobrecarda de operador
ostream& operator<<(ostream& os, const Output& out) {
  unsigned i;
  unsigned j;

  os << "graph{"<<endl;

  for (i = 0; i < out.in->cidadesGet(); i++) {
    for (j = i+1; j < out.in->cidadesGet(); j++) {
      if(out.cicle[i][j])
      os << i+1<< " -- "<< j+1<<endl;
    }
  }
  os << "}";
  return os;
}