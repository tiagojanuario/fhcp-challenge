#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <string>
#include <fstream>
#include <vector>
using namespace std;

#ifndef FHCP_CHALLENGE_INPUT_H
#define FHCP_CHALLENGE_INPUT_H

class Input {

	// Sobrecarga de operador
	friend ostream& operator<<(ostream&, const Input &in);

public:

	// Constructor padrao
	Input();

	// Constructor com instância como parâmetro
	Input(const string& file_name){
		load(file_name);
	}

	//Leitura e armazenamento dos dados de entrada
	void load(const string& file_name);

	// Testa se duas cidades estao diretamente conectadas
	bool estaoConectados(unsigned cidade_i, unsigned cidade_j) const {
		return matriz[cidade_i][cidade_j];
	}

	//! It returns the number of teams in the tournament
	unsigned cidadesGet() const {
		return cidades_;
	}

	//! Matriz simetrica das cidades conectadas
	vector<vector<unsigned> > matriz;
private:

	//! O numero total de cidades
	unsigned cidades_;

};

#endif  // FHCP_CHALLENGE_